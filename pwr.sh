[[ -f /sys/class/power_supply/BAT0/charge_now ]] || exit 1

left=`cat /sys/class/power_supply/BAT0/charge_now`
draw=`cat /sys/class/power_supply/BAT0/current_now`
left=$(($left/1000))
draw=$(($draw/1000))
echo " $left  $draw"
