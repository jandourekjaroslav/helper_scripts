#!/bin/bash

switch_out(){
	pactl set-default-sink $choice
	for i in ${outputApps[*]}
	do
		pactl move-sink-input $i  $choice
	done
}
switch_func(){
device_list="$@"
ar=()
lol=""
for i in $(seq 1 ${#device_list})
do
	if [ "${device_list:i-1:1}" = '\n' ] ||  [ "${device_list:i-1:1}" = " " ] || [ $i -eq ${#device_list} ]
	then

		if [ $i -eq ${#device_list} ]
		then

			lol=$lol${device_list:$i-1:1}
		fi
		ar[${#ar[@]}]=$lol'\n';
		lol=""
	else

		lol=$lol${device_list:$i-1:1}
	fi
done


}
#IFS=' '
inp=$(pactl list sinks| grep Name | sed 's/\tName: //g')
outputDevices=()
switch_func $inp
ar+=("output:hdmi-stereo+input:analog-stereo\n");
ar+=("output:analog-stereo+input:analog-stereo\n");
outputApps=$(pactl list sink-inputs | grep Sink | sed 's/Sink Input # //g')
choice=$(echo -e ${ar[*]} | dmenu -fn "Firacode:size=11" -nb "#282828" -nf "#d5c4a1" -sb "#0d3138" -sf "#d4be98" -p "Which output ?" )
choice=$(echo $choice | sed 's/ //g')
if [[ "$choice" == *"output:"* ]]
then
	pactl set-card-profile alsa_card.pci-0000_00_1f.3 $choice
else
	switch_out
fi
