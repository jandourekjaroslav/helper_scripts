#!/bin/bash
# Spawn N terminals in X working directory (fuzzy autojump)
count=${1}
dir=${2}
openDir=""
[[ -z ${1} ]] && count=2
[[ -z ${2} ]] && openDir=$(pwd) && fullPath=true

[[ $fullPath ]] || dir=$(autojump -s "" | grep $dir | sort -n -r | head -n 1 | cut -d ':' -f 2| sed 's/^\s*//g')
[[ "$dir" != "" ]] && openDir=$dir
for i in `seq 1 $count`
do
  if [[ "$openDir" != "" ]]
  then
    alacritty --working-directory $openDir &>/dev/null &
    disown
  else
    alacritty &>/dev/null &
    disown
  fi
done
